## Teste de Seleção 
# **Elano Barreto**

## Passos para configurar
1. Abrir o projeto no **VS Code**
2. Para rodar a api, entre no terminal e digitar "**_npm start_** "
3. Para rodar o app, abrir um novo terminal e digitar: 
    1. **_cd .\src\api_**
    2. **_node .\api-contrato.js_** 

## Passos para acessar o sistema
1. Clicar em Exibir contratos para ver todos os contratos atuais(Criei 3 mockados apeas para edição)
2. Caso queira criar um novo Contrato, clicar em "Criar novo contrato" e preencher as informações iniciais do contrato.
3. Caso queira criar uma parte, clicar em "Criar parte de contrato" e preencher as informações iniciais da parte.
4. Caso queira ediar um Contrato existente, clicar no link do contrato a direita.
5. Caso queira atrelar uma parte a um contrato:
    1. Escolher um contrato na lista e à direita, selecionar uma parte para anexar ao contrato.
6. É possível anexar um arquivo .pdf ou .doc no contrato e fazer o download do mesmo.

## Observações:
1. Devido ao tempo gasto no teste, não fiz as exclusões para completar os cruds
2. A visualização dos arquivos com as partes não foi possível, pois já estava com um tempo gasto muito anto no teste, mas creio que com um pouco mais de tempo teria dado para fechar todo o fluxo.
3. Usei Bootstrap como tema.