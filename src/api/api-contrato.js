var http = require('http'); 
const express = require('express');
const app = express();

app.use(require("cors")());
var bodyParser = require('body-parser');
const url = require('url');
const querystring = require('querystring');
//const path = require('path');

//app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

var contratos = [    
    { id: "1", titulo:'arquivo 1', dataInicio: '', dataVencimento: '', caminhoPdf: '', contratosParte: [] },
    { id: "2", titulo:'arquivo 2', dataInicio: '', dataVencimento: '', caminhoPdf: '', contratosParte: [] },
    { id: "3", titulo:'arquivo 3', dataInicio: '', dataVencimento: '', caminhoPdf: '', contratosParte: [] },
]

var contratosParte = [    
    { id: "1", idcontrato: "1", nome:'elano', sobrenome: 'barreto', email: 'elanofb@gmail.com', cpf:'99422034353', telefone: '85981951011'},
    { id: "2", idcontrato: "1", nome:'elise', sobrenome: 'bfb', email: 'elisebfb@gmail.com', cpf:'99422034353', telefone: '85981951011'},
    { id: "3", idcontrato: "2", nome:'maria', sobrenome: 'fontenele', email: 'zezefb@gmail.com', cpf:'99422034353', telefone: '85981951011'},
    { id: "4", idcontrato: "3", nome:'edvaldo', sobrenome: 'barreto', email: 'edvaldoab@gmail.com', cpf:'99422034353', telefone: '85981951011'},
]

////////////////////
// CRUD CONTRATOS //
////////////////////
app.get('/', (req, res, next) => {    
    console.log("Retornou todos os contratos!");
    res.json(contratos);
});

app.get('/getcontrato', (req, res, next) => {     
    //var id = req.body.id;    
    var id = req.query.id;
    //console.log("1 - " + id);
    let objResult = contratos.find(a => a.id === id);
    //console.log("2 -> " + JSON.stringify(objResult));
    //objResult.contratosParte = contratosParte.filter(a => a.idcontrato === objResult.id);
    //console.log("3 - " + id);
    console.log(JSON.stringify( objResult));
    res.json(objResult);
});
app.get('/retornarpartescontrato', (req, res, next) => {
    //console.log("Retornou todos os contratos!");
    res.json(contratosParte);
});

app.get('/getcontratoportitulo', (req, res, next) => {     
    var titulo = req.body.titulo;    
    let objResult = contratos.find(a => a.titulo === titulo);
    res.json(objResult);
});
app.get('/getpartecontrato', (req, res, next) => {     
    var id = req.query.id;    
    let objResult = contratosParte.find(a => a.id === id);
    console.log(JSON.stringify( objResult));
    res.json(objResult);
});

app.get('/getpartecontratopornome', (req, res, next) => {     
    var titulo = req.body.titulo;    
    let objResult = contratosParte.find(a => a.titulo === titulo);
    res.json(objResult);
});


app.post('/addcontrato', (req, res, next) => { 

    req.body.id = (contratos.length + 1).toString();
    req.body.contratosParte = []
    contratos.push(req.body);
    //res.json(contratos);
    console.log(JSON.stringify( contratos));
    res.json({message: "Tudo ok por aqui!", dados: req.body.id});
    //res.status(201).send(req.body);
});
app.put('/updatecontrato/', (req, res, next) => {
    const id = req.query.id;
    console.log("id: " + id);
    console.log(JSON.stringify(req.body));

    let objIndex = contratos.findIndex(a => a.id === id);
    console.log(JSON.stringify(objIndex));
    //contratos[objIndex].id = req.body.id;
    contratos[objIndex].titulo = req.body.titulo;
    contratos[objIndex].dataInicio = req.body.dataInicio;
    contratos[objIndex].dataVencimento = req.body.dataVencimento;
    contratos[objIndex].caminhoPdf = req.body.caminhoPdf;

    //contratos[objIndex] = req.body
    // res.status(201).send({
    //     id: titulo,
    //     item: objResult//req.body
    // });
    res.json(contratos);
});

///////////////////////////////
// CRUD PARTES DOR CONTRATOS //
///////////////////////////////


app.post('/addpartecontrato', (req, res, next) => { 

    req.body.id = (contratosParte.length + 1).toString();
    contratosParte.push(req.body);
    res.json(contratosParte);
    //res.status(201).send(req.body);
}); 

app.put('/updatecontrato/', (req, res, next) => {
    const id = req.query.id;
    console.log(id);
    let objIndex = contratosParte.findIndex(a => a.id === id);
    console.log(JSON.stringify(objIndex));
    contratosParte[objIndex].id = req.body.id;
    contratosParte[objIndex].titulo = req.body.titulo;
    contratosParte[objIndex].dataInicio = req.body.dataInicio;
    contratosParte[objIndex].dataVencimento = req.body.dataVencimento;
    contratosParte[objIndex].caminhoPdf = req.body.caminhoPdf;
    //contratos[objIndex] = req.body
    // res.status(201).send({
    //     id: titulo,
    //     item: objResult//req.body
    // });
    res.json(contratosParte);

});


app.put('/atrelarparteacontrato/', (req, res, next) => {
    const id = req.query.id;
    const idparte = req.query.idparte;

    //res.json({message: "Tudo ok por aqui!", dados: 'id:' + id + ', idparte:' + idparte});
    console.log(id  + ' - ' + idparte);
    let objIndex = contratos.findIndex(a => a.id === id);
    //var idparte = req.body.idparte;    
    console.log(JSON.stringify(contratosParte));
    let contratosParteObj = contratosParte.find(a => a.id === idparte);
    console.log(JSON.stringify(contratosParteObj));
    //console.log(JSON.stringify(objIndex));
    contratos[objIndex].contratosParte.push(contratosParteObj);
    //res.json(contratos);
    //console.log(JSON.stringify(contratos[objIndex].contratosParte));
    res.json({message: "Tudo ok por aqui!", dados: contratos[objIndex].contratosParte});
});

// router.post('/upload', function(req, res, next){    
//   var formidable = require('formidable');
//   var form = new formidable.IncomingForm();
//     form.parse(req, function (err, fields, files) {
//       res.write('File uploaded');
//       res.end();
//     });
// });
app.post('/upload', function(req, res, next){
    console.log('00');
    var formidable = require('formidable');
    var fs = require('fs');
    var form = new formidable.IncomingForm();
    console.log('01');
    form.parse(req, function (err, fields, files) {
        //console.log('01-1: ' + JSON.stringify(files.myFile));
        var oldpath = files.myFile.path;
        //var newpath = '../pdf/' + files.myFile.name;
        //C:\Users\elano\temp
        var newpath = '/pdf/' + files.myFile.name;
        fs.rename(oldpath, newpath, function (err) {
            if (err) throw err;
            res.write('File uploaded and moved!');
            res.end();
        });
        console.log('01-2');
    });
    console.log('02');
});

var server = http.createServer(app); 
server.listen(3030);
console.log("Servidor escutando na porta 3030...")