import React from 'react';
import {Route, BrowserRouter} from 'react-router-dom';

import Home from '../pages/Home';
import Form from '../pages/Form';
import CadastroParte from '../pages/CadastroParte';
import EditarArquivo from '../pages/EditarArquivo';
import ViewFile from '../pages/viewfile';
import ListarContratos from '../pages/listarContratos';


function Routes(){
    return (
        <BrowserRouter>
            <Route component={Home} path="/" exact />
            <Route component={Form} path="/cadastro" />
            <Route component={CadastroParte} path="/cadastroparte" />                        
            <Route component={EditarArquivo} path="/editararquivo" />
            <Route component={ViewFile} path="/viewfile" />    
            <Route component={ListarContratos} path="/listarContratos" />

        </BrowserRouter>
    )
}

export default Routes;