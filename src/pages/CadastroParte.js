import React, { useEffect, useState } from 'react';
import Header from '../Header';
import axios from 'axios';
import queryString from 'query-string';
import Bootstrap from "bootstrap/scss/bootstrap.scss";

function CadastroParte(){
    
    const [campos, setCampos] = useState({        
        id: '',
        nome: '',
        sobrenome: '',
        email: '',
        cpf: '',
        telefone: ''
    });
    const [partes, setPartes] = useState([]);
    
    function handleInputChange(event){
        campos[event.target.name] = event.target.value;
        setCampos(campos);
    }

    function handleFormSubmit(event){
        event.preventDefault();
        console.log(campos);
        axios.post('http://localhost:3030/addpartecontrato', campos).then(response => {
            window.location.href = "http://localhost:3000/ListarContratos";
        })
    }
  

    useEffect(() => {        
  
    }, []);

    return (
        <div class="col-md-12">

            <Header title="Criar partes de contrato" />

            <a class="btn btn-info" href="ListarContratos">Voltar para Lista de Contratos</a>
            
            <form onSubmit={handleFormSubmit}>
                <fieldset>                    
 
                    
                    <div class="row">
                        <div class="col-md-12">
                        <label>Nome:                            
                            <input type="text" name="nome" id="nome" onChange={handleInputChange} class="form-control" />
                        </label>
                        </div>
                    </div> 
                   
                    <div class="row">
                        <div class="col-md-12">
                        <label>Sobrenome:
                            <input type="text" name="sobrenome" id="sobrenome"  onChange={handleInputChange} class="form-control"/>
                        </label>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                        <label>Email:
                            <input type="text" name="email" id="email"  onChange={handleInputChange} class="form-control"/>
                        </label>
                        </div>
                    </div>

                    
                    <div class="row">
                        <div class="col-md-12">
                        <label>Cpf:
                            <input type="text" name="cpf" id="cpf"  onChange={handleInputChange} class="form-control"/>
                        </label>
                        </div>
                    </div>

                    
                    <div class="row">
                        <div class="col-md-12">
                        <label>Telefone:
                            <input type="text" name="telefone" id="telefone"  onChange={handleInputChange} class="form-control"/>
                        </label>
                        </div>
                    </div>
 
                    <div>
                        <table>
                            <thead>
                                <th>Id</th>
                                <th>Nome</th>
                                <th>Sobrenome</th>
                                <th>Email</th>
                                <th>Cpf</th>
                                <th>Telefone</th>
                                <th>-</th>
                            </thead>
                            <tbody>
                                {partes.map(parte => (
                                    <tr>
                                        <td>{parte.id}</td> 
                                        <td>{parte.nome}</td> 
                                        <td>{parte.sobreonme}</td>
                                        <td>{parte.email}</td>
                                        <td>{parte.cpf}</td>
                                        <td><a onclick="alert();">{parte.nome}</a></td>
                                    </tr>))}
                            </tbody>
                        </table>
                    </div>
                   
                    <input class="btn btn-primary" type="submit" value="Salvar" />
                    
                </fieldset>
            </form>
        </div>
    )
}

export default CadastroParte;