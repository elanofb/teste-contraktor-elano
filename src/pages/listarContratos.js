import React, { useEffect, useState } from 'react';
import Header from '../Header';
import axios from 'axios';
import queryString from 'query-string';
import {Link} from 'react-router-dom';
import DownloadLink from "react-download-link";
import Bootstrap from "bootstrap/scss/bootstrap.scss";

function ListarContratos(){
    
    const [contratos, setContratos] = useState([]);
   
    function CarregarDadosCampos(){       
        var parsedUrl = new URL(window.location.href)
        var idContrato = parsedUrl.searchParams.get('id');
        axios.get('http://localhost:3030/getcontrato?id='+ idContrato).then(response => {})
    }

    function PreencherCampo(idCampo,ValorCampo){
        document.getElementById(idCampo).value = ValorCampo;
    }

    function CarregarListaDeContratos(){
        var parsedUrl = new URL(window.location.href)
        var idContrato = parsedUrl.searchParams.get('id');
        axios.get('http://localhost:3030/getcontrato?id='+ idContrato).then(response => {
        })
    }

    function CarregarListaDePartes(){
        const tokenParam = queryString.parse(window.location.token);
        console.log(tokenParam);

        axios.get('http://localhost:3030/retornarpartescontrato').then(response => { })
    }   

    useEffect(() => {
        axios.get('http://localhost:3030/').then(response => {
            setContratos(response.data);            
        })
    }, []);

    return (

        <div class="col-md-12">

        <div class="table-responsive">
            <h1>Lista de Contratos</h1>
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <th>Nome</th>
                    <th>Sobrenome</th>
                    <th>Email</th>
                    <th>CPF</th>                                
                </thead>
                <tbody>
                    {contratos.map(contrato => (
                    <tr>                                        
                        <td>{contrato.id}</td>
                        <td>{contrato.titulo}</td>
                        <td>{contrato.caminhoPdf}</td>
                        <td><a href={"EditarArquivo?id=" + contrato.id}>{contrato.titulo}</a></td>
                        
                    </tr>))}
                </tbody>
            </table>

            <a class="btn btn-primary" href="cadastro">Criar novo Contrato</a>
            &nbsp;&nbsp;<a class="btn btn-warning" href="cadastroparte">Criar Parte de Contrato</a>
            
        </div>

        </div>

    )
    
}

export default ListarContratos;