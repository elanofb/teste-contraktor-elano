import React, {useState} from 'react';
import logo from '../logo.svg';
import Header from '../Header';
import {Link} from 'react-router-dom';

function Home() {

  const [contador, setContador] = useState(0);
  function increment(){
    setContador(contador+1);
  }

  return (
    <div className="App">
      <Header title="Gerenciamento de Contratos" />
      <header className="App-header">
        <Link to="/listarContratos">Exibir os contratos</Link><br></br>
        <img src={logo} className="App-logo" alt="logo" />      
      </header>
      
    </div>
  );
}

export default Home;
