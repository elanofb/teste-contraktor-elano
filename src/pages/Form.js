import React, { useEffect, useState } from 'react';
import Header from '../Header';
import axios from 'axios';
import queryString from 'query-string';
import {Link} from 'react-router-dom';

function Form(){
    
    const [campos, setCampos] = useState({
        titulo: '',
        dataInicio: '',
        dataVencimento: '',
        caminhoPdf: ''
    });
    const [estados, setEstados] = useState([]);
    const [partes, setPartes] = useState([]);
    
    function handleInputChange(event){
        campos[event.target.name] = event.target.value;
        setCampos(campos);
    }


    function handleFormSubmit(event){
        event.preventDefault();
        console.log(campos);
        axios.post('http://localhost:3030/addcontrato', campos).then(response => {
            //window.location.href = "http://localhost:3000/EditarArquivo?id=" + response.data.dados;
            window.location.href = "http://localhost:3000/ListarContratos";
        })
    }
  const [state, setState] = useState([]);
  
  var onFileChange = event => { 
    setState({ selectedFile: event.target.files[0] }); 
  };
  
  var onFileUpload = () => { 
  
    const formData = new FormData(); 
    
    formData.append( 
      "myFile", 
      state.selectedFile, 
      state.selectedFile.name 
    ); 
   
    console.log(state.selectedFile);    
    axios.post("http://localhost:3030/upload", formData); 
    
  }   

    useEffect(() => {
    }, []);

    return (
        <div class="col-md-12">
            <Header title="Dados do Contrato" />
            <a class="btn btn-info" href="ListarContratos">Voltar para Lista de Contratos</a>

            <form onSubmit={handleFormSubmit}>
                <fieldset>
 
                    <div class="row">
                        <div class="col-md-12">
                            <label>Título:
                            <input type="text" name="titulo" id="titulo" onChange={handleInputChange} class="form-control"/>
                            </label>
                        </div>
                    </div>
 
                    <div class="row">
                        <div class="col-md-12">
                            <label>Caminho Pdf:
                                <input type="text" name="caminhoPdf" id="caminhoPdf"  onChange={handleInputChange} disabled="disabled" class="form-control"/>
                            </label>
                        </div>
                    </div> 

                    <div class="row">
                        <div class="col-md-12">
                        <label>Data Início:
                            <input type="date" name="dataInicio" id="dataInicio"  onChange={handleInputChange} class="form-control"/>
                        </label>
                        </div>
                    </div> 

                    <div class="row">
                        <div class="col-md-12">
                        <label>Data Vencimento:
                            <input type="date" name="dataVencimento" id="dataVencimento"  onChange={handleInputChange} class="form-control"/>
                        </label>
                        </div>
                    </div>                    
                   
                   
                    <input type="submit" value="Salvar" class="btn btn-primary"/>
                    
                </fieldset>
            </form>
        </div>
    )
}

export default Form;