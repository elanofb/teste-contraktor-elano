import React, { useEffect, useState } from 'react';
import Header from '../Header';
import axios from 'axios';
import queryString from 'query-string';
import {Link} from 'react-router-dom';
import DownloadLink from "react-download-link";
import Bootstrap from "bootstrap/scss/bootstrap.scss";

function EditarArquivo(){
    
    const [campos, setCampos] = useState({
        titulo: '',
        dataInicio: '',
        dataVencimento: '',
        caminhoPdf: ''
    });
    const [contratos, setContratos] = useState([]);
    const [partes, setPartes] = useState([]);

    function CarregarDadosCampos(){       
        var parsedUrl = new URL(window.location.href)
        var idContrato = parsedUrl.searchParams.get('id');
        axios.get('http://localhost:3030/getcontrato?id='+ idContrato).then(response => {
            setContratos(response.data.contratosParte);

            PreencherCampo('titulo', response.data.titulo);
            PreencherCampo('caminhoPdf', response.data.caminhoPdf);
            PreencherCampo('dataVencimento', response.data.dataVencimento);            
            PreencherCampo('dataInicio', response.data.dataInicio);            
            
        })
    }

    function PreencherCampo(idCampo,ValorCampo){        
        document.getElementById(idCampo).value = ValorCampo;
    }

    function handleInputChange(event){
        AtualizarCamposUpdate();
        setCampos(campos);
    }

    function handleChangeAtrelarParte(event){

        var parsedUrl = new URL(window.location.href)
        const id = parsedUrl.searchParams.get('id');
        const idparte = event.target.value;
        event.preventDefault();
        axios.put('http://localhost:3030/atrelarparteacontrato?id=' + id + '&idparte=' + idparte, campos).then(response => {            
            setContratos(response.data.dados);
        })
    }

    function handleFormSubmit(event){

        var parsedUrl = new URL(window.location.href)
        const id = parsedUrl.searchParams.get('id');
        event.preventDefault();
        console.log(campos);
        axios.put('http://localhost:3030/updatecontrato?id=' + id, campos).then(response => {
            //CarregarListaDeContratos();
            window.location.href = "http://localhost:3000/ListarContratos";
        })

    }

    function CarregarListaDeContratos(){
        var parsedUrl = new URL(window.location.href)
        var idContrato = parsedUrl.searchParams.get('id');
        axios.get('http://localhost:3030/getcontrato?id='+ idContrato).then(response => {
            setContratos(response.data.contratosParte);
        })
    }

    function CarregarListaDePartes(){
        const tokenParam = queryString.parse(window.location.token);
        console.log(tokenParam);

        axios.get('http://localhost:3030/retornarpartescontrato').then(response => {
            setPartes(response.data);
        })
    }

    const [state, setState] = useState([]);

    var onFileChange = event => { 
        setState({ selectedFile: event.target.files[0] });        
    };
   
    var onFileUpload = () => { 
        const formData = new FormData(); 
   
        formData.append( 
        "myFile", 
        state.selectedFile, 
        state.selectedFile.name 
        ); 

        console.log(state.selectedFile);    
        axios.post("http://localhost:3030/upload", formData); 
    }
   
    function AtualizarCamposUpdate(){
        if(state.selectedFile){
            campos["caminhoPdf"] = state.selectedFile.name;
        }
        else{
            campos["caminhoPdf"] = document.getElementById("caminhoPdf").value;
        }
        campos["titulo"] = document.getElementById("titulo").value;
        campos["dataInicio"] = document.getElementById("dataInicio").value;
        campos["dataVencimento"] = document.getElementById("titulo").value;
    }

    function fileData(){
           
        if (state.selectedFile) { 
            document.getElementById("caminhoPdf").value = state.selectedFile.name;
            AtualizarCamposUpdate();
        } 

    }    

    useEffect(() => {
        CarregarListaDeContratos();
        CarregarListaDePartes();
        CarregarDadosCampos();
    }, []);

    return (
        <div class="col-md-12">
            <Header title="Gerenciar Contrato" />
            <a class="btn btn-info" href="ListarContratos">Voltar para Lista de Contratos</a>

            <form onSubmit={handleFormSubmit}>
                
                    
                <div class="row">

                    <div class="col-md-6">
                    
                    <h3>Dados do Contrato</h3>

                    <fieldset>

                    <div class="row">
                        <div class="col-md-12">
                        <label>Título:                            
                            <input type="text" name="titulo" id="titulo" onChange={handleInputChange} class="form-control"/>
                        </label>
                        </div> 
                    </div>
 
                    <div class="row">
                        <div class="col-md-12">
                        <label>Caminho Pdf:                            
                            <input type="text" name="caminhoPdf" id="caminhoPdf"  onChange={handleInputChange} disabled="disabled" class="form-control"/>
                        </label>
                        </div> 
                    </div> 

                    <div class="row">
                        <div class="col-md-12">
                        <label>Data Início:
                            <input type="date" name="dataInicio" id="dataInicio"  onChange={handleInputChange} class="form-control"/>
                        </label>
                        </div> 
                    </div> 

                    <div class="row">
                        <div class="col-md-12">
                        <label>Data Vencimento:
                            <input type="date" name="dataVencimento" id="dataVencimento"  onChange={handleInputChange} class="form-control"/>
                        </label>
                        </div> 
                    </div> 

                    <div class="row">
                        <div class="col-md-12">
                        <label>
                            <Link id="linkDownload" to="C:\pdf\CV_Elano.Barreto_202.pdf" target="_blank" download>Download Arquivo</Link>                            
                        </label>
                        </div> 
                    </div>

                    <div>                         
                        <div> 
                            <input type="file" onChange={onFileChange} /> 
                            <button onClick={onFileUpload}> 
                            Upload Arquivo
                            </button> 
                            <br></br> <span class="bg-grey"><small><em>Salvo em: "c:\pdf\"</em></small></span>
                        </div> 
                        {fileData()} 
                    </div> 

                    <input type="submit" value="Salvar" class="btn btn-primary" />                        

                    </fieldset>
                    </div>

                    <div class="col-md-6">


                        <h3>Partes do Contrato</h3>

                        <label>Escolha uma parte para anexar ao contrato
                            <select name="cmbUF" id="cmbUF" onChange={handleChangeAtrelarParte} class="form-control">
                                <option value="0">Selecione uma parte</option>
                                {partes.map(parte => (<option key={parte.id} value={parte.id}>{parte.nome}</option>))}
                            </select>
                        </label>


                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <th>Nome</th>
                                <th>Sobrenome</th>
                                <th>Email</th>
                                <th>CPF</th>                                
                            </thead>
                            <tbody>
                                {contratos.map(contrato => (
                                    <tr>                                        
                                        <td>{contrato.nome}</td>
                                        <td>{contrato.sobreonme}</td>
                                        <td>{contrato.email}</td>
                                        <td><a onclick="alert();">{contrato.cpf}</a></td>
                                    </tr>))}
                            </tbody>
                        </table>

                    </div>                   
                    
                </div>


                
            </form>
        </div>
    )
}

export default EditarArquivo;